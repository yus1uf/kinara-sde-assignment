import mongoose from "mongoose";


const studentSchema = new mongoose.Schema({
  roll_number: {
    type: String,
    required: true,
    unique: true,
  },
  name: {
    type: String,
    required: true,
  },
  total_marks: {
    type: Number
  }
});



const User = mongoose.model("Student", studentSchema);

export default User;
