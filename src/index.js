import express from "express";
import { DBConnection } from "./database/connection/dbConnection.js";
import router from "./routes/index.js";

const app = express();

const port = process.env.PORT || 8085;

app.use(express.json());

app.use(
  express.urlencoded({
    extended: true,
  })
);

app.use("/api/v1", router);

app.use("*", (req, res) => {
  return res.status(200).json({
    app: "Kinara Capital SDE Home Assignment",
    delployedUrl: "https://kinara-sde-assignment.onrender.com/api/v1",
    devUrl: "http://localhost:4000/api/v1",
    routes: [
      { 
        path: "/students?<YOUR_QUERY>",
        queryParams: ["page","pageSize"],
        example: "https://kinara-sde-assignment.onrender.com/api/v1/students?page=1&pageSize=50"
     },
     { 
      path: "/students/filter?<YOUR_QUERY>",
      queryParams: ["name","sortBy","sortOrder"],
      example: "https://kinara-sde-assignment.onrender.com/api/v1/students/filter?name=joh&sortBy=total_marks&sortOrder=desc"
   }
  ],
  });
});

app.listen(port, () => {
  DBConnection();
  console.log(`app listening on port ${port}`);
});
