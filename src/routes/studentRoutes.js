import express from "express";
import {
getStudents,
filterStudents
} from "../controller/studentController.js";


const studentRouter = express.Router();


studentRouter.get("/", getStudents);
studentRouter.get("/filter", filterStudents);



export default studentRouter;
