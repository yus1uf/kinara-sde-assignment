export const Messages = {
  SERVER_ERROR: "something went wrong!!",
  NOT_FOUND: "no note found in database!!",
  FETCHED: "student details fetched successfully!!",

};

export const Codes = {
  SUCCESS: 200,
  NOT_FOUND: 404,
  SERVER_ERROR: 500,
};
