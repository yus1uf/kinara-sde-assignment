import Student from "../database/model/studentModel.js";
import { Codes, Messages } from "../utils/constant.js";
import { customResponse } from "../utils/helper.js";

export const getStudents = async (req, res, next) => {
  try {
    const { page = 1, pageSize = 10 } = req.query;
    const skip = (page - 1) * pageSize;

    const students = await Student.find().skip(skip).limit(Number(pageSize));
    const successResponse = customResponse({
      code: Codes.SUCCESS,
      message:Messages.FETCHED,
      data: students
    })
    return res.status(Codes.SUCCESS).send(successResponse)
  } catch (error) {
   console.log(error);
   const errorResponse = customResponse({
    code: Codes.SERVER_ERROR,
    message:Messages.SERVER_ERROR,
    err: error
  })
  return res.status(Codes.SERVER_ERROR).send(errorResponse)
  }
};

// Server-side Filtering API
export const filterStudents = async (req, res) => {
  try {
    const { name, sortBy, sortOrder } = req.query;


    const filter = {};
    if (name) {
      filter.name = { $regex: new RegExp(name, 'i') };
    }

    const sort = {};
    if (sortBy) {
      sort[sortBy] = sortOrder === 'desc' ? -1 : 1;
    }

    const filteredStudents = await Student.find(filter).sort(sort);
    const successResponse = customResponse({
      code: Codes.SUCCESS,
      message:Messages.FETCHED,
      data: filteredStudents
    })
    return res.status(Codes.SUCCESS).send(successResponse)
  } catch (error) {
    console.log(error);
    const errorResponse = customResponse({
     code: Codes.SERVER_ERROR,
     message:Messages.SERVER_ERROR,
     err: error
   })
   return res.status(Codes.SERVER_ERROR).send(errorResponse)
  }
};