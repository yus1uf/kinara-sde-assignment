# Kinara Capital SDE Home Assignment


#### Documents Updates
| Version | Create | Update |
| :------ | :------| :------|
| `0.0.2` | `Jan 22, 2024` | `Jan 22, 2024` |


## Environment and Requirements

To run this project, you will need to add have

| Environment   | Version                                                                    |
| -----------   | -------------------------------------------------------------------------- |
| Node.js       | [Node.js](https://nodejs.org/) v20.10.\*                                   |
| npm           | [npm](https://docs.npmjs.com/try-the-latest-stable-version-of-npm) v10.2.*\* |


## Documentation 

**Server:** Node.js(Express)


The [server directory](./src) consist of the back-end code.

#### Backend-end tech stack

- Express v4 (Server and APIs)
- Mongoose

## Refer Below for API Documentation
 - [Click Here](https://documenter.getpostman.com/view/17963881/2s9YymGQAK) || https://documenter.getpostman.com/view/17963881/2s9YymGQAK

### Deployed Server Link
## [Click](https://kinara-sde-assignment.onrender.com/api/v1) [https://kinara-sde-assignment.onrender.com/api/v1]

## Defined Routes

  - /students?page=1&pageSize=50
  - /students/filter?name=joh&sortBy=total_marks&sortOrder=desc

## Run Locally

Clone the project (via HTTP)

```bash
git clone --single-branch -b main https://gitlab.com/yus1uf/kinara-sde-assignment.git
```

Go to the project directory

```bash
  For backed-end:
  cd kinara-sde-assignment
```

Install dependencies(for back-end)

```bash
npm install
```

Start the server(for back-end)

```bash
  npm run start
```